# Copyright 2013 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=mariusmuja ] cmake [ api=2 ]

SUMMARY="Fast library for approximate nearest neighbors"
DESCRIPTION="
FLANN is a library for performing fast approximate nearest neighbor searches in
high dimensional spaces. It contains a collection of algorithms we found to
work best for nearest neighbor search and a system for automatically choosing
the best algorithm and optimum parameters depending on the dataset.
FLANN is written in C++ and contains bindings for the following languages: C,
MATLAB and Python."

HOMEPAGE+=" http://www.cs.ubc.ca/research/${PN}/"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="doc examples openmp"

DEPENDENCIES="
    build:
        doc? (
            dev-tex/biblatex-biber
            dev-texlive/texlive-latex
        )
    build+run:
        examples? ( sci-libs/hdf5 )
        openmp? ( sys-libs/libgomp:* )
    test:
        dev-cpp/gtest
        sci-libs/hdf5
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_C_BINDINGS:BOOL=true
    -DBUILD_CUDA_LIB:BOOL=false
    -DBUILD_MATLAB_BINDINGS:BOOL=false
    # feel free to enable that if you need it
    -DBUILD_PYTHON_BINDINGS:BOOL=false
    -DDOCDIR="/usr/share/doc/${PNVR}"
    -DUSE_MPI:BOOL=false
)
CMAKE_SRC_CONFIGURE_OPTIONS=( 'openmp USE_OPENMP' )
CMAKE_SRC_CONFIGURE_OPTION_BUILDS=(
    DOC
    EXAMPLES
)
CMAKE_SRC_CONFIGURE_TESTS+=( '-DBUILD_TESTS:BOOL=TRUE -DBUILD_TESTS:BOOL=FALSE' )

src_prepare() {
    cmake_src_prepare

    # Work around a build failure with cmake[>=3.11]
    # https://github.com/mariusmuja/flann/issues/369
    edo touch src/cpp/empty.cpp
    edo sed -e "/add_library(flann_cpp SHARED/ s/\"\"/empty.cpp/" \
            -e "/add_library(flann SHARED/ s/\"\"/empty.cpp/" \
            -i src/cpp/CMakeLists.txt
}

src_install() {
    default

    if option examples ; then
        insinto /usr/share/doc/${PNVR}
        doins -r "${CMAKE_SOURCE}"/examples/
    fi
}

